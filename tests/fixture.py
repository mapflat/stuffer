import logging
import os
import shlex
import shutil
import sys
import unittest
from pathlib import Path

from click.testing import CliRunner

from stuffer import main
from stuffer.core import run_cmd
from stuffer.utils import str_split


TEST_IMAGE = "stuffer_test_image"
TEST_CONTAINER = "stuffer_test_ctr"


class DockerTest(unittest.TestCase):
    RUN_LOCAL = False  # True is useful for interactive debugging.

    def setUp(self):
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG,
                            format='%(asctime)s %(levelname)-7s %(message)s',
                            datefmt='%y-%m-%d %H:%M:%S')
        self.remove_container()
        src_dir = Path(__file__).parents[1].resolve()
        os.system(" ".join(["docker", "build", "--tag", TEST_IMAGE, str(src_dir)]))
        run_cmd(["docker", "run", "--detach", "--name", TEST_CONTAINER,
                 "--volume={}:/stuffer_src".format(str(src_dir)), TEST_IMAGE, "sleep", "10000"])
        run_cmd(["docker", "exec", TEST_CONTAINER, "./setup.py", "sdist"])
        tarball = run_cmd(["docker", "exec", TEST_CONTAINER, "ls", "dist/"]).strip()
        run_cmd(["docker", "exec", TEST_CONTAINER, "pip3", "install", "dist/" + tarball])

    def tearDown(self):
        pass
        # self.remove_container()
        # self.remove_image()

    @staticmethod
    def remove_container():
        if run_cmd(["docker", "ps", "--all", "--quiet", "--filter", "name={}".format(TEST_CONTAINER)]) != "":
            run_cmd(["docker", "rm", "--force", TEST_CONTAINER])

    @staticmethod
    def remove_image():
        if run_cmd(["docker", "images", "--quiet", TEST_IMAGE]) != "":
            run_cmd(["docker", "rmi", "--force", TEST_IMAGE])

    def stuff(self, commands, extra_args=None):
        stuffer_store = "/tmp/stuffer_test_store"
        full_commands = ["--store-dir", stuffer_store, "--verbose"] + (extra_args or []) + commands
        if self.RUN_LOCAL:
            shutil.rmtree(stuffer_store, ignore_errors=True)
            return self._stuff_locally(full_commands)
        return self.container_run(["stuffer"] + full_commands)

    @staticmethod
    def _stuff_locally(commands):
        runner = CliRunner()
        logging.info("> stuffer {}".format(" ".join([shlex.quote(c) for c in commands])))
        result = runner.invoke(main.cli, commands, catch_exceptions=False)
        if result.exit_code != 0:
            logging.error(result.output)
        else:
            logging.debug(result.output)
        assert result.exit_code == 0
        return result.output

    @staticmethod
    def container_run(commands):
        return run_cmd(["docker", "exec", "--tty=false", TEST_CONTAINER] + str_split(commands))
