import sys

from pathlib import Path


sys.path.append(str(Path(__file__).parents[1]))

import fixture


class DockerTest(fixture.DockerTest):
    def test_prologue(self):
        self.stuff(['docker.Prologue()'])
        self.assertRegex(self.container_run(['cat', '/var/log/stuffer.log']), r'phusion')

    def test_epilogue(self):
        self.stuff(['apt.Install("pchar")'])
        self.stuff(['docker.Epilogue()'])
        # For very mysterious reasons, the directory /var/lib/apt/lists/partial does not get removed on Shippable.
        # It is not important, so test for absence of files instead.
        self.assertEqual(self.container_run(['find', '/var/lib/apt/lists', '/var/cache/apt', '-type', 'f']).strip(), "")
