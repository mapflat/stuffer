#! /usr/bin/env python3
import hashlib
import logging
import os
import re
import subprocess
import sys

import requests


def abort(msg):
    logging.error(msg)
    raise RuntimeError(msg)


def pypirc(location):
    if os.path.exists(location):
        logging.info("%s already exists", location)
    else:
        logging.info("Creating %s", location)
        open(location, 'w').write(("\n".join(
            ["[distutils]",
             "index-servers=",
             "  pypi",
             "  testpypi",
             "",
             "[pypi]",
             "username = lalle",
             "",
             "[testpypi]",
             "repository = https://test.pypi.org/legacy/",
             "username = lalle",
             ""])))


def validate_version(version):
    if not re.match(r"^\d+\.\d+\.\d+$", version):
        abort("Invalid version: '{}'".format(version))
    logging.info("Checking whether version flag also prints %s", version)
    stuffer_env = os.environ.copy()
    # noinspection PyTypeChecker
    stuffer_env['PYTHONPATH'] = ':'.join(['.'] + os.environ.get('PYTHONPATH', '').split(':'))
    cli_version = subprocess.check_output(["python3", "stuffer/main.py", "--version"],
                                          env=stuffer_env).decode().strip()
    if cli_version != version:
        abort("stuffer/main.py reports version {}, did you forget to update".format(cli_version))


def is_version_new(version):
    logging.info("Checking whether version %s already exists", version)
    pypi_page = requests.get('https://pypi.python.org/simple/stuffer/').content.decode()
    exists = pypi_page.find('stuffer-{}.tar.gz'.format(version)) != -1
    if exists:
        logging.info("Version {} already exists on pypi.python.org".format(version))
    else:
        logging.info("Version %s does not already exist, release party", version)
    return not exists


def main(argv):
    logging.basicConfig(level=logging.INFO)

    # noinspection PyTypeChecker
    pypirc("{}/.pypirc".format(os.environ["HOME"]))
    logging.info("Determining release version")
    version = subprocess.check_output(["./setup.py", "--version"], universal_newlines=True).strip()
    validate_version(version)
    if is_version_new(version):
        return upload_release('--test' in argv, version)
    else:
        return 0


def upload_release(test_repo, version):
    repo = 'testpypi' if test_repo else 'pypi'
    logging.info("Performing release upload to %s", repo)
    logging.info("Environment keys: %s", os.environ.copy().keys())
    logging.info("Password hash: %s", hashlib.sha256(os.environ['TWINE_PASSWORD'].encode()).hexdigest())
    upload = "twine upload --verbose --repository={} dist/stuffer-{}.tar.gz dist/stuffer-{}-py3-none-any.whl".format(
        repo, version, version)
    logging.info(upload)
    result = os.system(upload)
    if result:
        logging.error("Release upload failed with exit code %d", result)
    else:
        logging.info("Release upload of version %s succeeded", version)
    return result


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
