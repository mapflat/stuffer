import getpass
import os
from pathlib import Path


class Distribution:
    """Information about the system distribution."""

    @classmethod
    def _lsb_release(cls):
        key_vals = [r.strip().split('=', 1) for r in Path('/etc/lsb-release').open()]
        return {k: v.strip('"') for k, v in key_vals}

    @classmethod
    def codename(cls):
        return cls._lsb_release()["DISTRIB_CODENAME"]

    @classmethod
    def release(cls):
        return cls._lsb_release()['DISTRIB_RELEASE']


def real_user():
    return os.environ.get('SUDO_USER', getpass.getuser())
