"""
Package installation with snap commands.

Packages are installed with snap.Install.
"""

from stuffer.content import StringArg
from stuffer.core import Action, run_cmd


class Install(Action):
    """Install a package with snap install.

    Parameters
    ----------
    package
        Name of package. Standard ``snap install`` version constraints can be used, e.g. ``wget=1.17.1``.
    """

    def __init__(self, package: StringArg, classic: bool = False):
        self.package = package
        self.classic = classic
        super(Install, self).__init__()

    def run(self) -> None:
        run_cmd(["snap", "install", self.package, *(["--classic"] if self.classic else [])])
