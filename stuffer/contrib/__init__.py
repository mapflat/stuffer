"""Package with installers for third-party packages and non-core functionality."""

from stuffer.contrib import dropbox
from stuffer.contrib import google
from stuffer.contrib import hashicorp
from stuffer.contrib import kafka
from stuffer.contrib import nettools
from stuffer.contrib import spotify
