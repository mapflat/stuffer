from typing import List

from stuffer.core import Group, Action

from stuffer import snap


class SpotifyClient(Group):
    def children(self) -> List[Action]:
        return [snap.Install("spotify")]
