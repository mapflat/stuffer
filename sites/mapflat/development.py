from stuffer import apt
from stuffer import content
from stuffer import files
from stuffer import pip
from stuffer import snap
from stuffer import system
from stuffer import user
from stuffer.contrib import hashicorp
from stuffer.core import Group


class Azure(Group):
    def children(self):
        return [
            apt.KeyAdd("https://packages.microsoft.com/keys/microsoft.asc"),
            apt.SourceList("azure-cli",
                           "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ " +
                           f"{system.Distribution.codename()} main"),
            apt.Install("azure-cli")
            ]


class Docker(Group):
    def children(self):
        docker_compose = "/usr/local/bin/docker-compose"
        docker_compose_version = "1.6.2"
        docker_compose_complete = "/etc/bash_completion.d/docker-compose"
        return [apt.Install('docker.io'),
                user.AddToGroup(system.real_user(), 'docker'),
                files.DownloadFile(
                    "https://github.com/docker/compose/releases/download/{}/docker-compose-Linux-x86_64".format(
                        docker_compose_version),
                    docker_compose),
                files.Chmod(0o755, docker_compose),
                files.DownloadFile(
                    "https://raw.githubusercontent.com/docker/compose/{}/contrib/completion/bash/docker-compose".format(
                        docker_compose_version),
                    docker_compose_complete),
                files.Chmod(0o644, docker_compose_complete),
                ]


class Google(Group):
    def children(self):
        return [apt.KeyAdd("https://packages.cloud.google.com/apt/doc/apt-key.gpg"),
                apt.SourceList("kubernetes", "deb https://apt.kubernetes.io/ kubernetes-xenial main"),
                apt.Install('kubectl'),
                apt.Install('lsb-release')]


class Network(Group):
    def children(self):
        return [apt.Install(pkg) for pkg in [
            'bind9-host',
            'dnsutils',
            'httrack',
            'iputils-ping',
            'lsof',
            'netcat-openbsd',
            'net-tools',
            'speedtest-cli',
            'strace',
            'tcptrack',
        ]]


class Podman(Group):
    def children(self):
        release = system.Distribution.release()
        return [
            apt.SourceList(
                'devel:kubic:libcontainers:stable',
                        'deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/'
                        f'xUbuntu_{release} /'),
            apt.KeyAdd('https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/'
                       f'xUbuntu_{release}/Release.key'),
            apt.Install('podman')
            ]

class PythonLibs(Group):
    def children(self):
        return [pip.Install('beautifulsoup4')]


class Sbt(Group):
    def children(self):
        return [
            apt.SourceList('sbt', 'deb https://repo.scala-sbt.org/scalasbt/debian all main'),
            apt.KeyRing("scalasbt-release", 'https://keyserver.ubuntu.com/pks/lookup?op=get&' +
                       'search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823'),
            apt.Install('sbt')
            ]
        
    
class SysAdmin(Group):
    def children(self):
        return [apt.Install(pkg) for pkg in [
            'lastpass-cli',
        ]] + [hashicorp.Terraform('1.0.11')]


class Tools(Group):
    def children(self):
        return [apt.Install('autoconf'),
                apt.Install('automake'),
                apt.Install('ca-certificates-java'),
                apt.Install('direnv'),
                apt.Install('git-core'),
                apt.Install('golang-go'),
                apt.Install('groovy'),
                snap.Install('intellij-idea-community', classic=True),
                apt.Install("jq"),
                apt.Install('kdiff3'),
                apt.Install('libmysqlclient-dev'),
                # These libs are desirable when compiling python
                apt.Install('libncurses5-dev'),
                apt.Install('libbz2-dev'),
                apt.Install('liblzma-dev'),
                apt.Install('libreadline-dev'),
                apt.Install('libsqlite3-dev'),
                apt.Install('maven'),
                apt.Install('mercurial'),
                apt.Install('mysql-client'),
                apt.Install('nodejs'),
                pip.Install('nose'),
                apt.Install('openjdk-8-jdk'),
                # apt.Install('openjdk-11-jdk'),
                apt.Install('packer'),
                apt.Install('postgresql'),
                apt.Install('postgresql-server-dev-all'),
                apt.Install('python-dev'),
                apt.Install('python3-dev'),
                apt.Install('python3-doc'),
                apt.Install('python3-mysqldb'),
                apt.Install('python3-venv'),
                pip.Install("restview"),
                apt.Install('ruby'),
                apt.Install('scala'),
                apt.Install('scala-doc'),
                apt.Install('scons'),
                apt.Install('subversion'),
                apt.Install("tox"),
                pip.Install('twine'),
                apt.Install("vagrant"),
                apt.Install("virtualbox"),
                ]
