from stuffer import apt
from stuffer import debconf
from stuffer.core import Group


class Graphics(Group):
    def children(self):
        return [
            apt.Install('gphoto2'),
            apt.Install('gpick'),
            apt.Install('gthumb'),
            apt.Install('pinta'), 
        ]

class Kde(Group):
    def children(self):
        return [
            # https://bugs.launchpad.net/ubuntu/+source/kaccounts-providers/+bug/1488909
            # ShellCommand("dpkg --remove account-plugin-google unity-scope-gdrive"),
            apt.Install("kubuntu-desktop"),
            apt.Install('kde-style-oxygen-qt5'),
            apt.Install("konqueror"),
            debconf.SetSelections('ttf-mscorefonts-installer', 'msttcorefonts/accepted-mscorefonts-eula',
                                  'select', 'true'),
            apt.Install('kubuntu-restricted-extras'),
        ]
