from stuffer import apt

apt.AddRepository('ppa:ubuntugis/ubuntugis-unstable')
apt.Install(['gdal-bin', 'python-gdal'])
apt.Install(['libimage-exiftool-perl'])

