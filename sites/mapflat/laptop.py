import logging
import re
import sys
from pathlib import Path

from stuffer import apt
from stuffer import files
from stuffer import pip
from stuffer import snap
from stuffer import system
from stuffer.contrib import dropbox
from stuffer.contrib import google
from stuffer.contrib import nettools
from stuffer.contrib import spotify


# Add the sites directory to path in order to 'import mapflat.<module>'
site_root = str(Path(__file__).parent.parent)
logging.debug("Adding site root directory to path: %s", site_root)
sys.path.append(site_root)

from mapflat import development
from mapflat import desktop

if system.Distribution.codename() != "impish":
    apt.AddRepository('multiverse')

    development.Azure()


development.Google()
development.Network()

apt.KeyAdd("https://download.01.org/gfx/RPM-GPG-KEY-ilg-3")

apt.KeyRecv("hkp://keyserver.ubuntu.com:80", "642AC823")
# apt.SourceList("sbt", "deb https://dl.bintray.com/sbt/debian /")

# Disabled automatic unattended upgrades by overriding 20auto-upgrades.
files.Content("/etc/apt/apt.conf.d/90no-auto-upgrades", 'APT::Periodic::Unattended-Upgrade "0";\n')

google.ChromeStable()

apt.Install(['emacs'])

dropbox.DropboxClient()

google.NameServer()
apt.Install('sudo')
files.Content("/etc/sudoers.d/nopasswd", "%sudo ALL=NOPASSWD: ALL\n")

# For gsutil
apt.Install(['libffi-dev', 'libssl-dev'])
pip.Install('cryptography')

# Development
development.PythonLibs()
development.Sbt()
development.SysAdmin()
development.Tools()
development.Docker()
# Podman conflicts with kubectl on the package container-common.
#development.Podman()

pip.Install("awscli")

apt.Install('ubuntu-session')

# debconf.SetSelections('lightdm', 'shared/default-x-display-manager', 'select', 'sddm')
# debconf.SetSelections('sddm', 'shared/default-x-display-manager', 'select', 'sddm')

desktop.Graphics()
desktop.Kde()

apt.Install('aspell-sv')
apt.Install('calibre')
apt.Install('graphviz')
apt.Install('gv')
# Need specific version for mapflat site. We don't support "apt-mark hold" yet, so manage manually.
# apt.Install('hugo')
apt.Install("pandoc")
apt.Install('ttf-xfree86-nonfree')
apt.Install("xclip")

apt.Install('psutils')
apt.Install('texlive-latex-base')
apt.Install('texlive-fonts-recommended')
apt.Install('texlive-lang-european')
apt.Install('tgif')

apt.Install('kaffeine')
apt.Install(['mplayer', 'mplayer-skins', 'smplayer'])

apt.Install('tesseract-ocr')

spotify.SpotifyClient()

# development.DebugTools()
# development.NetDebugTools()
apt.Install("acpi")
pip.Install("bpytop")
apt.Install('cpufrequtils')
snap.Install("fx")
apt.Install(['gkrellm', 'gkrelltop'])
apt.Install("htop")
apt.Install("libmbim-utils")
apt.Install("pm-utils")
apt.Install("ripgrep")
apt.Install('tlp')
apt.Install('tree')

nettools.Wireshark()
apt.Install("openvpn-systemd-resolved")

# Needed for Intel graphipipcs installer
apt.Install("ttf-ancient-fonts")

files.Transform("/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf",
                lambda c: re.sub(r"user-session=.*", "user-session=kde-plasma", c))

# Necessary for sddm login screen to work properly
apt.Purge("gdm3")
apt.Purge('thunderbird')
