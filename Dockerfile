# This is 18.04 latest per 2019-03-10
FROM ubuntu:bionic-20190204

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install --yes apt-utils && apt-get -f --yes install
RUN apt-get install --yes python3-pip
RUN pip3 install --upgrade pip

RUN mkdir /stuffer
COPY . /stuffer
WORKDIR /stuffer
